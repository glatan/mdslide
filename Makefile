NAME    = mdslide
SRC_DIR = ./src
LDFLGS  = -a -tags netgo -installsuffix netgo -buildmode=pie -ldflags '-w -s'

default: build

build:
	go build ${LDFLGS} -o ${NAME} ${SRC_DIR}
